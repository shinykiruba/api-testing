package com.FrameworkSample.api.test;

import static org.testng.Assert.assertNotNull;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.FrameworkSample.api.helpers.PersonServiceHelper;

public class TestPATCHPerson {
	private PersonServiceHelper personServiceHelper;
	@BeforeClass
	public void init() {
		personServiceHelper = new PersonServiceHelper();
	}
	
	@Test
	public void patchAPost() {
		String id = personServiceHelper.patchAPost(3).jsonPath().getString("id");
		System.out.println(id);
		assertNotNull(id,"Updated");
	}
}
