package com.FrameworkSample.api.test;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.FrameworkSample.api.helpers.PersonServiceHelper;

public class TestDELETEPerson {
	private PersonServiceHelper personServiceHelper;
	@BeforeClass
	public void init() {
		personServiceHelper = new PersonServiceHelper();
	}
	
	@Test
	public void deleteAPost() {
		personServiceHelper.deleteAPost(3);
	}
}
