package com.FrameworkSample.api.test;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.FrameworkSample.api.helpers.PersonServiceHelper;
import static org.testng.Assert.assertNotNull;

public class TestPOSTPerson {
private PersonServiceHelper personServiceHelper;
	
	@BeforeClass
	public void init() {
		personServiceHelper = new PersonServiceHelper();
	}
	
	@Test
	public void createAPost() {
		String id = personServiceHelper.createAPost().jsonPath().getString("id");
		System.out.println(id);
		assertNotNull(id,"Posted");
	}
}
