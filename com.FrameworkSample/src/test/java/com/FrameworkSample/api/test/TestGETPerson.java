package com.FrameworkSample.api.test;


import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

import com.FrameworkSample.api.helpers.PersonServiceHelper;
import com.FrameworkSample.api.model.Post;

import io.restassured.response.Response;

public class TestGETPerson {
	private PersonServiceHelper personServiceHelper;
	
	@BeforeClass
	public void init() {
		personServiceHelper = new PersonServiceHelper();
	}
	
	@Test
	public void testGetAllPost()
	{
		List<Post> postList = personServiceHelper.getAllPost();
		assertNotNull(postList,"Post List is not empty");
		assertFalse(postList.isEmpty(),"Post List is not true");
	}
	
	@Test
	public void testGetSinglePerson()
	{
		Response postList = personServiceHelper.getSinglePost();
		assertNotNull(postList,"Post List is not empty");
	}
}
