package com.FrameworkSample.api.constants;

public class EndPoints {
	public static final String GET_ALL_POST ="/posts";
	public static final String GET_SINGLE_POST ="/posts/{id}";
	public static final String CREATE_POST ="/posts";
	public static final String UPDATE_POST ="/posts/{id}";
	public static final String DELETE_A_POST ="/posts/{id}";
}
