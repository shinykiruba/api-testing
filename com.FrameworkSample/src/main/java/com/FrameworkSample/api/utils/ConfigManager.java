package com.FrameworkSample.api.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigManager {
	
	private static final Properties prop = new Properties();
	
	private ConfigManager() throws IOException    
	{
		FileInputStream fis = new FileInputStream("/com.FrameworkSample/resources/config.properties");
			
				prop.load(fis);
	}
	
	
	public static String getString(String key) {
		return System.getProperty(key, prop.getProperty(key));
	}
}
