package com.FrameworkSample.api.helpers;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertEquals;

import java.lang.reflect.Type;
import java.util.List;

import org.apache.http.HttpStatus;

import com.FrameworkSample.api.constants.EndPoints;
import com.FrameworkSample.api.model.Post;
import com.fasterxml.jackson.core.type.TypeReference;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class PersonServiceHelper {
	// we need to read the config variables
	// Rest Assured about the URL, Port
	// Make a Get REQUEST on this url and send the data to TestGETPerson
	private static final String BASE_URL = "http://localhost:3000";
	
	
	public PersonServiceHelper() {
		RestAssured.baseURI = BASE_URL;
		RestAssured.useRelaxedHTTPSValidation();
	}
	
	public List<Post> getAllPost(){
		Response response = RestAssured
				.given().log().all()
				.contentType(ContentType.JSON)
				.get(EndPoints.GET_ALL_POST)
				.andReturn();
		response.prettyPrint();
		Type type = new TypeReference<List<Post>>(){}.getType();
		List<Post> postList = response.as(type);
		assertEquals(response.statusCode(),HttpStatus.SC_OK,"Ok");
		return postList;
}
	public Response getSinglePost(){
		Response response = RestAssured
				.given()
				.pathParam("id","2" )
				.contentType(ContentType.JSON)
				.get(EndPoints.GET_SINGLE_POST)
				.andReturn();
		response.prettyPrint();
		assertEquals(response.statusCode(),HttpStatus.SC_OK,"Ok");
		return response;
		
	}
	
	public Response createAPost() {
		Post post = new Post();
		post.setId(3);
		post.setAuthor("Smilien Ebina");
		post.setTitle("Word of God");
		Response response = RestAssured
				.given()
				.contentType(ContentType.JSON)
				.when()
				.body(post)
				.post(EndPoints.CREATE_POST)
				.andReturn();
		assertEquals(response.statusCode(),HttpStatus.SC_CREATED,"Created");
		response.prettyPrint();
		return response;
	}
	
	public Response patchAPost(Integer id) {
		Post post = new Post();
		post.setAuthor("Golda kiruba");
		post.setTitle("God is Love");
		Response response = RestAssured
				.given()
				.contentType(ContentType.JSON)
				.pathParam("id", id)
				.when()
				.body(post)
				.patch(EndPoints.UPDATE_POST)
				.andReturn();
		assertTrue(response.statusCode()== HttpStatus.SC_OK);
		response.prettyPrint();
		return response;
				
	}
	public Response deleteAPost(Integer id) {
		Response response = RestAssured
				.given()
				.contentType(ContentType.JSON)
				.pathParam("id", id)
				.delete(EndPoints.DELETE_A_POST)
				.andReturn();
		assertTrue(response.statusCode()== HttpStatus.SC_OK);
		return response;
	}
}
